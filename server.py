# -*- coding: utf-8 -*-

from twisted.application import service, internet
from twisted.web import static, server
import sys, os
from twisted.python.threadpool import ThreadPool
from twisted.web.server import Site
from twisted.web.resource import Resource
from twisted.web import wsgi

from twresource import Root

PROJECT_ROOT = os.path.dirname(__file__)
SRC_DIR = os.path.join(PROJECT_ROOT, "src")
sys.path.append(SRC_DIR)
print "SRC_DIR", SRC_DIR

from cserver import GameFactory

SOCKET_PORT = 8006
WEBB_PORT = 8000

gameFactory = GameFactory()

os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'
from django.core.handlers.wsgi import WSGIHandler
#
#def wsgi_resource():
#    pool = ThreadPool()
#    pool.start()
#    # Allow Ctrl-C to get you out cleanly:
#    # reactor.addSystemEventTrigger('after', 'shutdown', pool.stop)
#    wsgi_resource = wsgi.WSGIResource(reactor, pool, WSGIHandler())
#    return wsgi_resource

if __name__ == '__main__':
    from twisted.internet import reactor
    
    print "Start from __main__"
    
    reactor.listenTCP(SOCKET_PORT, gameFactory)

#    wsgi_root = wsgi_resource()
#    root = Root(wsgi_root)
#    website = server.Site(root)
#    reactor.listenTCP(WEBB_PORT, root)
    #internet.TCPServer(PORT, main_site).setServiceParent(application)
    reactor.run()
else:
    print "Start from twisted"
    application = service.Application("battlechess")
    internet.TCPServer(SOCKET_PORT, gameFactory).setServiceParent(application)
    
#    wsgi_root = wsgi_resource()
#    root = Root(wsgi_root)
#    website = server.Site(root)
#    internet.TCPServer(WEBB_PORT, root).setServiceParent(application)