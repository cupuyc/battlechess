import socket               # Import socket module

try:
    s = socket.socket()
    host = socket.gethostname()
    port = 8006

    s.connect((host, port))
    s.sendall("stop_password")
    print "Stopped successfully          !"
except Exception as error:
    print "Server not started", error