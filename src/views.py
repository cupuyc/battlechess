from django.http import HttpResponse
from django.template import RequestContext
from django.shortcuts import render_to_response

def home(request):
    #facebook_profile = request.user.get_profile().get_facebook_profile()
    
    return render_to_response('home.html',
                              { 'facebook_profile': None },
                              context_instance=RequestContext(request))

#def index(request):
#    return HttpResponse("Hello, world. You're at the poll index.")
