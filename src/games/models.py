from django.db import models
import ChessBoard
#from djangotoolbox.fields import ListField

class Game(models.Model):
    board = models.CharField(max_length=500)
    #pub_date = models.DateTimeField('date published')

    def __unicode__(self):
        return self.board

class UserVO(models.Model):
    name = models.CharField(max_length=200)
    coins = models.PositiveIntegerField()


class MatchVO(models.Model):
    board = models.CharField(max_length=500)
    #history = ListField()
    #comments = ListField()
    whiteUserId = models.PositiveIntegerField()
    blackUserId = models.PositiveIntegerField()

    # transient
    chess = None

    def reset(self):
        self.chess = ChessBoard.ChessBoard()
        self.chess.resetBoard()
        self.board = self.chess.getBoard()

    def hasPlayer(self, userId):
        return self.whiteUserId == userId or self.blackUserId == userId
