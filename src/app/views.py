from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template import RequestContext
from socialregistration.contrib.facebook.models import FacebookProfile

def index(request):
    return render_to_response(
        'index.html', dict(
            facebook=FacebookProfile.objects.all(),
#            foursquare=FoursquareProfile.objects.all(),
#            github=GithubProfile.objects.all(),
#            linkedin=LinkedInProfile.objects.all(),
#            openid=OpenIDProfile.objects.all(),
#            tumblr=TumblrProfile.objects.all(),
#            twitter=TwitterProfile.objects.all(),
    ), context_instance=RequestContext(request))
