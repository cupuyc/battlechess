from django.views.generic.simple import direct_to_template
from django.conf.urls.defaults import patterns, include, url
from django.views.generic.simple import redirect_to
from app import views;


# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    #url(r'^$', 'views.home', name='home'),

    
    # from django_facebook_oauth
    #url(r'^$', 'example.views.home'),

    #url(r'^facebook/login$', 'facebook.views.login'),
    #url(r'^facebook/authentication_callback$', 'facebook.views.authentication_callback'),
    #url(r'^logout$', 'django.contrib.auth.views.logout'),
    # end

    #url(r'^app/', direct_to_template, {'template': 'index.html'}),
    url(r'^$', 'app.views.index', name='index'),
    url(r'^social/', include('socialregistration.urls', namespace = 'socialregistration')),
    
    # workarond url after login
    # http://battlechess.trembit.com:8000/accounts/profile/#_=_
    url(r'^accounts/profile/$', redirect_to, {'url': '/#play'}),


    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),

    # Uncomment the admin/doc line below to enable admin documentation:
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
)
