# -*- coding: UTF-8 -*-

from twisted.internet import reactor
from twisted.internet.protocol import Factory, Protocol

import random
import string

# amfast
from amfast.encoder import Encoder
from amfast.decoder import Decoder
from amfast import class_def

# DJANGO TRICK
from os.path import dirname
import os, sys
import logging
from twisted import internet

        
logging.basicConfig(
    level=logging.DEBUG,
    format='%(asctime)s %(levelname)-5.5s [%(name)s] %(message)s'
)

# HACK: Django doesn’t directly support the use of the ORM outside of
# a Django project. If this module is imported outside of a project
# some environment needs to be set up. See
# http://jystewart.net/process/2008/02/using-the-django-orm-as-a-standalone-component/.
if "DJANGO_SETTINGS_MODULE" not in os.environ:
    sys.path.insert(0, (dirname(__file__) or ".") + "/../..") # put settings.py on path
    os.environ['DJANGO_SETTINGS_MODULE'] = "settings"
    print "Django settings injected"
else:
    print "Found var ", os.environ["DJANGO_SETTINGS_MODULE"]
    os.environ["DJANGO_SETTINGS_MODULE"] = "settings"


from games.models import MatchVO
from django.contrib.sessions.models import Session
from django.contrib.auth.models import User

class ServerAction():

    def __init__(self, actionArg=None, dataArg=None):
        self.action = actionArg
        self.data = dataArg

    @staticmethod
    def __new__(cls=None, classObj=None, classobj=None):
        return ServerAction()

class UserVO():

    LOBBY = 0;
    SEARCHING = 1;
    PLAYING = 2;

    def __init__(self, idArg=None, nameArg=None, stateArg=0):
        self.id = idArg
        self.name = nameArg
        self.state = stateArg

#pyamf.register_class(ServerAction, "ServerAction")
#pyamf.register_class(UserVO, "UserVO")
#pyamf.register_class(MatchVO, "MatchVO")

class Game(Protocol):
    #encoding = pyamf.AMF3    # pyamf

    #amfast static vars
    class_mapper = class_def.ClassDefMapper()
    class_mapper.mapClass(class_def.DynamicClassDef(ServerAction, "ServerAction"))
    class_mapper.mapClass(class_def.DynamicClassDef(MatchVO, "MatchVO"))
    class_mapper.mapClass(class_def.DynamicClassDef(UserVO, "UserVO"))
    encoder = Encoder(amf3=True)
    decoder = Decoder(amf3=True, class_def_mapper=class_mapper)
    encoder.class_def_mapper = class_mapper

    def __init__(self):
        #pyamf
        #self.encoder = pyamf.get_encoder(self.encoding)
        #self.stream = self.encoder.stream
        
        self.id = None
        self.name = None
        self.state = UserVO.LOBBY


    def dataReceived(self, data):
        if data.find("<policy-file-request") > -1:
            with open('src/crossdomain.xml', 'rb') as f:
                response = f.read() + '\0'
                self.transport.write(response)
                print "Sent answer", response
        elif data.find("stop_password") == 0:
            print "Stop server requested!"
            reactor.stop()
        else:
            serverAction = self.decoder.decode(data, True)
            print "Client action", self.id, serverAction.action, serverAction.data
            handler = getattr(self.factory, "doAction" + serverAction.action)
            if handler != None:
                handler(self, serverAction.data)
            else:
                print "Unknown action from client", serverAction.action

    def lineReceived(self, data):
        print "ERROR: Unimplemented"
        # do nothing
        #self.factory.sendAll("%s" % (data))
        pass

    def getId(self):
        return str(self.transport.getPeer())

    def connectionMade(self):
        print "User join:", self.getId()
        #self.transport.write("Connected！\n")

    def connectionLost(self, reason):
        print "User left", self.getId(), reason
        self.factory.delClient(self)

    ### HELPERS ###

    def getRandomString(self):
        char_set = string.ascii_uppercase + string.digits
        return ''.join(random.sample(char_set,6))

    def sendToClient(self, data):
        #self.encoder.writeElement(data)
        #self.transport.write(self.stream.getvalue())
        #self.stream.truncate()
        print "Send to client " + data.action
        self.transport.write(self.encoder.encode(data))
    def getUser(self):
        return UserVO(self.id, self.name, self.state)


class GameFactory(Factory):
    protocol = Game

    def __init__(self):
        self.pendingClients = set()
        self.clients = set()
        self.searchingClients = set()
        self.playingClients = set()
        self.matches = set()

        self.timer = reactor.callLater(2, self.checkMatching)

        #print "Connect to redis..."
        #r_server = redis.Redis("localhost")
        #r_server.set("name", "BattleChess")
        #print "Server started", r_server.get("name")

        # MongoDB connection
        #self.connection = Connection()
        #self.db = self.connection.battlechess_db
        #self.games_collection = self.db.games
        print "Server started, matches in history:", MatchVO.objects.count()

    def getPlayerId(self):
        return len(self.player)

    def getClients(self):
        return [client.getUser() for client in self.clients];

    def addClient(self, newclient):
        self.clients.add(newclient)

        self.setUserState(newclient, UserVO.SEARCHING)
        self.updateAllClient()

    def delClient(self, client):
        self.setUserState(client, UserVO.LOBBY)
        self.clients.discard(client)
        for match in self.matches:
            if match.hasPlayer(client.id):
                print "User left in match ", client.id

        self.updateAllClient()

    def setUserState(self, client, state=0):
        client.state = state;
        self.playingClients.discard(client)
        self.searchingClients.discard(client)

        if state == UserVO.SEARCHING:
            self.searchingClients.add(client)
        if state == UserVO.PLAYING:
            self.playingClients.add(client)

    def doActionIntroduce(self, client, ticket):
        print "doActionIntroduce ticket", ticket
        user = None
        try:
            session = Session.objects.get(session_key=ticket)
            uid = session.get_decoded().get('_auth_user_id')
            user = User.objects.get(pk=uid)
        except Exception as error:
            print "Error retriving user, disconnect socket", error
            client.transport.loseConnection()
        
        print "Introduced user is " + user.username
        client.id = user.id;
        client.name = user.username;
        self.addClient(client)

    def doActionMakeTurn(self, client, turn):
        playerMatch = None
        for match in self.matches:
            if match.hasPlayer(client.id):
                playerMatch = match
                break

        if playerMatch and turn:
            moveResult = playerMatch.chess.addTextMove(turn)
            playerMatch.board = playerMatch.chess.getBoard()
            if moveResult == False:
                print "Move failed", turn, playerMatch.chess.getReason()
            else:
                playerMatch.save()  # save to db
                print "Move accepted", turn

            self.updateMatch(playerMatch)

    def checkMatching(self):
        try:
            if self.searchingClients.__len__() > 1:
                client1 = self.searchingClients.pop()
                client2 = self.searchingClients.pop()
                match = MatchVO()
                match.reset()
                match.whiteUserId = client1.id
                match.blackUserId = client2.id
                match.save()
                self.matches.add(match)
                self.setUserState(client1, UserVO.PLAYING)
                self.setUserState(client2, UserVO.PLAYING)
                self.updateMatch(match)
                self.updateAllClient()
        except Exception as error:
            print "Error during match", error
            
        self.timer = reactor.callLater(2, self.checkMatching)

    def updateMatch(self, match):
        action = ServerAction("match", match)
        for client in self.clients:
            if match.hasPlayer(client.id):
                client.sendToClient(action) 

    def updateAllClient(self):
        self.sendToAllClients(ServerAction("users", self.getClients()))

    def sendToAllClients(self, data):
        for client in self.clients:
            client.sendToClient(data)

#print dir(proto)
def stop_battlechess():
    try:
        reactor.stop()
        print "Stop successfull!"
    except:
        print "stop died!"

#internet.TCPServer(8006, factory).setServiceParent(application)
#reactor.listenTCP(8006, GameFactory())
#reactor.run()
